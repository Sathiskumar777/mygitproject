package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelBasic {

	public static void main(String[] args) throws IOException {
		XSSFWorkbook wbook = new XSSFWorkbook("./Data/TC001.xlsx");
		XSSFSheet sheet = wbook.getSheet("Sheet1");
		
		int rowNum = sheet.getLastRowNum();
		short cellNum = sheet.getRow(0).getLastCellNum();
		for (int i = 1; i <= rowNum; i++) {
			XSSFRow row = sheet.getRow(i);	
			for (int j = 0; j < cellNum; j++) {
				XSSFCell cell = row.getCell(j);
				String text = cell.getStringCellValue();
				System.out.println(text);
			}
		}
		
	}

}
