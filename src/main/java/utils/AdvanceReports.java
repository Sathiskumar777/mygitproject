package utils;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class AdvanceReports {
	static ExtentHtmlReporter html;
	static ExtentReports extent;
	ExtentTest test;
	
	public void startReport() {
		html = new ExtentHtmlReporter("./reports/myTestReports.html");
		extent = new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
	}

	public void assignTest(String testcaseName, String testDec, String author, String category) {		
		test = extent.createTest(testcaseName, testDec);
		test.assignAuthor(author);
		test.assignCategory(category);
	}

	public void logTest(String desc, String status) {
		if(status.equalsIgnoreCase("pass")) {
			test.pass(desc);
		}else if(status.equalsIgnoreCase("fail")) {
			test.fail(desc);
		}else if(status.equalsIgnoreCase("warning")) {
			test.warning(desc);
		} 
		}

	public void endReport() {
		extent.flush();		
	}
}
