package bl.framework.testcases;

import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC002_DuplicateLead extends ProjectMethods{
	
	@BeforeTest(groups = "common")
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testDec = "Testcase one is to create a new Test lead";
		author = "Sathis";
		category = "Sanity";
	}
	
@Test(groups = "reg")

	public void Duplicate() throws InterruptedException{
	//Launch browser wsith URL
/*	startApp("chrome", "http://leaftaps.com/opentaps");
	
	//Login
	WebElement eleUser = locateElement("id", "username");
	clearAndType(eleUser, "DemoSalesManager");
	WebElement elePass = locateElement("id", "password");
	clearAndType(elePass, "crmsfa");
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin);
	
	WebElement eleCRM = locateElement("linktext", "CRM/SFA");
	click(eleCRM);
*/	
	WebElement eleLeads = locateElement("linktext", "Leads");
	click(eleLeads);
	
	WebElement eleFindLeads = locateElement("linktext","Find Leads");
	click(eleFindLeads);
	
	WebElement eleEmail = locateElement("xpath","//span[text()='Email']");
	click(eleEmail);	
	WebElement eleEmailAddr = locateElement("name", "emailAddress");
	clearAndType(eleEmailAddr, "test@test.com");
	WebElement eleFindButton = locateElement("xpath", "//button[text()='Find Leads']");
	click(eleFindButton);
	Thread.sleep(2000);
	
	//Name of first resulting lead
	WebElement eleTable = locateElement("xpath","(//table[@class = 'x-grid3-row-table'])//td[3]");
	String orgName = eleTable.getText();
	System.out.println(orgName);
	
	//click first resulting lead
	WebElement eleFirstLead = locateElement("xpath","(//table[@class = 'x-grid3-row-table'])//a");
	click(eleFirstLead);
	
	//click duplicate lead and verify page name
	WebElement eleDup = locateElement("linktext", "Duplicate Lead");
	click(eleDup);
	WebElement eleTitle = locateElement("id", "sectionHeaderTitle_leads");
	System.out.println(eleTitle.getText());
	
	//createlead button
	WebElement eleSubmit = locateElement("name", "submitButton");
	click(eleSubmit);
	
	//confirm duplicated lead name is same as captured name
	WebElement eleDupName = locateElement("id", "viewLead_firstName_sp");
	String dupName = eleDupName.getText();
	if(dupName.equals(orgName)) 
		System.out.println("Duplicated lead name is same as captured name");
	else
		System.out.println("Duplicated lead name is not same as captured name");
	
	//close the browser
	close();
	quit();
}


}