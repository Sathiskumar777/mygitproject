package bl.framework.testcases;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bl.framework.design.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{

@BeforeTest(groups = "common")
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testDec = "Testcase one is to create a new Test lead";
		author = "Sathis";
		category = "Sanity";
}

@Test(/*groups = "smoke"*/dataProvider = "getData2")
	public void create(String cname, String fname, String lname) {
		//CreateLead
		WebElement eleCreate = locateElement("linktext", "Create Lead");
		click(eleCreate);

		//get visible text
		WebElement eleTitle = locateElement("id", "sectionHeaderTitle_leads");
		System.out.println(getElementText(eleTitle));

		//Mandatory Details
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompanyName, /*"CTS"*/cname);
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFirstName, /*"Sathis"*/fname);
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLastName, /*"R"*/lname);
		WebElement eleSubmit = locateElement("name", "submitButton");
		click(eleSubmit);

		//view lead
		String text = locateElement("id", "viewLead_firstName_sp").getText();
		if(text.equals("Sathya")) {
			System.out.println("Lead has been created for Sathya");
		}

	}
@DataProvider(name = "getData1")
public String[][] fetchdata(){
	String[][] data = new String[2][3];
	data[0][0] = "Cognizant";
	data[0][1] = "Sathis";
	data[0][2] = "A";
	
	data[1][0] = "Cognizant";
	data[1][1] = "Anbu";
	data[1][2] = "B";
	return data;	
}

@DataProvider(name = "getData2")
public String[][] fetchdataone(){
	String[][] data = new String[2][3];
	data[0][0] = "Cognizant";
	data[0][1] = "Sathis";
	data[0][2] = "A";
	
	data[1][0] = "Cognizant";
	data[1][1] = "Anbu";
	data[1][2] = "B";
	return data;
}

}
