package bl.framework.testcases;

import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.SendKeysAction;

import bl.framework.api.SeleniumBase;

public class TC001_LoginAndLogout extends SeleniumBase{

	@Test
	public void login() {
		//opening browser and logging in
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
	//	WebElement eleLogout = locateElement("class", "decorativeSubmit");
	//	click(eleLogout);
		
		//Creating Lead
		WebElement crmsfa = locateElement("linktext", "CRM/SFA");
		click(crmsfa);
		
		WebElement createLead = locateElement("linktext", "Create Lead");
		click(createLead);
		
		WebElement company = locateElement("id", "createLeadForm_companyName");
		clearAndType(company, "COGNIZANT");
		
		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(firstName, "Sathis kumar");
		
		WebElement lastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(lastName, "R");
		
		WebElement submit = locateElement("name", "submitButton");
		click(submit);	
		
		//Handling Alerts
		
	}
}








