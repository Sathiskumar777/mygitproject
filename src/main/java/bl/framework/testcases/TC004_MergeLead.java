package bl.framework.testcases;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.SendKeysAction;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC004_MergeLead extends ProjectMethods{
	
	@BeforeTest(groups = "common")
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testDec = "Testcase one is to create a new Test lead";
		author = "Sathis";
		category = "Sanity";
	}


@Test(groups = "sanity")
	public void Merge() throws InterruptedException {
/*		startApp("chrome", "http://www.leaftaps.com/opentaps");
		
		WebElement eleuser = locateElement("id", "username");
		clearAndType(eleuser, "DemoSalesManager");
		WebElement elepass = locateElement("id", "password");
		clearAndType(elepass, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		
		WebElement eleCRM = locateElement("linktext", "CRM/SFA");
		click(eleCRM);
*/		
		WebElement eleLeads = locateElement("linktext", "Leads");
		click(eleLeads);
		
		WebElement eleMerge = locateElement("xpath", "//a[text() = 'Merge Leads']");
		click(eleMerge);
		
		WebElement eleFrom = locateElement("xpath", "(//img[@alt = 'Lookup'])[1]");
		click(eleFrom);
		
		switchToWindow(1);
		
		WebElement eleLeadIdFrom = locateElement("name", "id");
		eleLeadIdFrom.sendKeys("10015");
		
		WebElement eleFindFrom = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(eleFindFrom);
		Thread.sleep(2000);
		
		WebElement eleFirstLeadFrom = locateElement("xpath", "//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		click(eleFirstLeadFrom);
		
		switchToWindow(0);
		
		WebElement eleTo = locateElement("xpath", "(//img[@alt ='Lookup'])[2]");
		click(eleTo);
		
		switchToWindow(1);
		
		WebElement eleLeadIdTo = locateElement("name", "id");
		eleLeadIdTo.sendKeys("10024");
		
		WebElement eleFindTo = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(eleFindTo);
		Thread.sleep(2000);
		
		WebElement eleFirstLeadTo = locateElement("xpath", "//td[@class = 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a");
		click(eleFirstLeadTo);
		
		switchToWindow(0);
		
		WebElement eleMergeAction = locateElement("linktext", "Merge");
		click(eleMergeAction);
		
		acceptAlert();
		
		WebElement ele = locateElement("linktext", "Find Leads");
		click(ele);
		
		WebElement eleToId = locateElement("name", "id");
		eleToId.sendKeys("10015");
		
		WebElement eleClick = locateElement("xpath", "//button[text() = 'Find Leads']");
		click(eleClick);
		
		WebElement eleError = locateElement("xpath", "//div[contains(text(),'No records to display')]");
		verifyDisplayed(eleError);
		getElementText(eleError);
		
		close();
		
} 
}
