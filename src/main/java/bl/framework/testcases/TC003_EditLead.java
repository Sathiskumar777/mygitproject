package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC003_EditLead extends ProjectMethods{
	
@BeforeTest(groups = "common")
	public void setData() {
		testcaseName = "TC003_EditLead";
		testDec = "Testcase one is to create a new Test lead";
		author = "Sathis";
		category = "Sanity";
	}
	
@Test(/*dependsOnMethods="bl.framework.testcases.TC001_CreateLead.create"*/groups = "smoke")

public void edit() throws InterruptedException {
	startApp("chrome", "http://leaftaps.com/opentaps");
	
	/*WebElement eleName = locateElement("id", "username");
	clearAndType(eleName, "DemoSalesManager");
	WebElement elePass = locateElement("id", "password");
	clearAndType(elePass, "crmsfa");
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin);
	
	WebElement eleCRM = locateElement("linktext", "CRM/SFA");
	click(eleCRM);*/
	
	WebElement eleLeads = locateElement("linktext", "Leads");
	click(eleLeads);
	
	WebElement eleFind = locateElement("linktext", "Find Leads");
	click(eleFind);
	
	WebElement eleFirst = locateElement("xpath", "(//input[@name = 'firstName'])[3]");
	clearAndType(eleFirst, "RAVI");
	
	WebElement eleSubmit = locateElement("xpath", "//button[text() ='Find Leads']");
	click(eleSubmit);
	Thread.sleep(2000);
	
	WebElement ele = locateElement("xpath", "//table[@class = 'x-grid3-row-table']//a");
	click(ele);
	
	WebElement eleTitle = locateElement("id", "sectionHeaderTitle_leads");
	System.out.println(eleTitle.getText());
	
	WebElement eleEdit = locateElement("linktext", "Edit");
	click(eleEdit);
	
	WebElement eleUpdate = locateElement("id", "updateLeadForm_companyName");
	clearAndType(eleUpdate, "Cognizant-MEPZ");
	
	WebElement eleSub = locateElement("name", "submitButton");
	click(eleSub);
	
	String eleVerify = locateElement("id", "viewLead_companyName_sp").getText();
	if(eleVerify.contains("Cognizant-MEPZ"))
		System.out.println("Company name is updated");
	else
		System.out.println("Company name is not updated");
	
	close();
	}

}

