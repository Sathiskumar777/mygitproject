package bl.framework.testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

@Test
public class Week5Day1ReportsBasic {
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	
	public void runReport() {
			html = new ExtentHtmlReporter("./reports/Extent reports.html");
			extent = new ExtentReports();
			html.setAppendExisting(true);
			extent.attachReporter(html);	
		ExtentTest test = extent.createTest("TC001_Login", "Login into the system");
		test.assignAuthor("Sathis");
		test.assignCategory("Smoke");
		try {
		test.pass("Login is successful", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		}catch(IOException e) {
			e.printStackTrace();
		}
		extent.flush();
	}
	
	
	

}
