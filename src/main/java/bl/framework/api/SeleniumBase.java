package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import bl.framework.base.Browser;
import bl.framework.base.Element;
import utils.AdvanceReports;

public class SeleniumBase extends AdvanceReports implements Browser, Element{

	public RemoteWebDriver driver;
	public int i =1;
	@Override
	public void startApp(String url) {
		try {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.get(url);
		} catch (WebDriverException e) {
			System.out.println("Webdriver Exception occurred");
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver(); 
			}
			driver.manage().window().maximize();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The browser "+browser+" launched successfully");
		} catch (WebDriverException e) {
			System.out.println("Webdriver Exception occurred");
		}
		finally {
        takeSnap();
		}
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch (locatorType) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "xpath": return driver.findElementByXPath(value);
			case "linktext": return driver.findElementByLinkText(value);
			case "tagname": return driver.findElementByTagName(value);
			case "partiallinktext": return driver.findElementByPartialLinkText(value);
			case "css": return driver.findElementByCssSelector(value);
			default:
				break;
			}
		} catch (NotFoundException e) {
			System.out.println("The value "+value+" is not found");
		}catch (WebDriverException e) {
			System.out.println("Webdriver exception occurred");
		}finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		try {
			WebElement elementId = driver.findElementById(value);
			if(elementId!=null) {
				return elementId;
			}else {
			return null;
			}
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		try {
			switch(type) {
			case "id": return driver.findElementsById(value);
			case "name": return driver.findElementsByName(value);
			case "class": return driver.findElementsByClassName(value);
			case "xpath": return driver.findElementsByXPath(value);
			case "linktext": return driver.findElementsByLinkText(value);
			case "tagname": return driver.findElementsByTagName(value);
			case "partiallinktext": return driver.findElementsByPartialLinkText(value);
			case "css": return driver.findElementsByCssSelector(value);
			default: break;
			}
		} catch (NotFoundException e) {
			System.out.println("The value "+value+" is not found");
		}catch (WebDriverException e) {
			System.out.println("Webdriver exception occurred");
		}
		return null;
	}

	@Override
	public void switchToAlert() {
		Alert alert = driver.switchTo().alert();
	}

	@Override
	public void acceptAlert() {
		try {
			Alert alert = driver.switchTo().alert();
			String text = alert.getText();
			alert.accept();
			System.out.println("The alert "+text+" has been accepted");
		} catch (NoAlertPresentException e) {
			System.out.println("No Alert is present");
		} catch (WebDriverException e) {
			System.out.println("Webdriver exception occurred");
		}
	}

	@Override
	public void dismissAlert() {
		try {
			Alert alert = driver.switchTo().alert();
			String text = alert.getText();
			alert.dismiss();
			System.out.println("The alert "+text+" has been dismissed");
		} catch (NoAlertPresentException e) {
			System.out.println("No Alert is present");
		} catch (WebDriverException e) {
			System.out.println("Webdriver exception occurred");
		}
	}

	@Override
	public String getAlertText() {
		try {
			String alertText = driver.switchTo().alert().getText(); 
			if(alertText!=null)
				return alertText;
		} catch (NoAlertPresentException e) {
			System.out.println("Alert is not present");
		} catch(WebDriverException e) {
			System.out.println("Webdriver exception occurred");
		}
		return null;
	}

	@Override
	public void typeAlert(String data) {
		try {
			Alert alert = driver.switchTo().alert();
			alert.sendKeys(data);
		} catch (NoAlertPresentException e) {
			System.out.println("Alert is not present");
		} catch(WebDriverException e) {
			System.out.println("Webdriver exception occurred");
		}
	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			List<String> ls = new ArrayList<>();
			ls.addAll(allWindows);
			driver.switchTo().window(ls.get(index));
			System.out.println("Switched to the "+index+ " window successfully");
		} catch (NoSuchWindowException e) {
			System.out.println("The window is not found");
		}  catch (WebDriverException e) {
			System.out.println("Webdriver exception occurred");
		}
	}

	@Override
	public void switchToWindow(String title) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			for (String window : allWindows) {
				driver.switchTo().window(window);
				if(driver.getTitle().equals(title)) {
					break;
				}
			}
			System.out.println("Switched to the window "+title+" successfully");
		} catch (NoSuchWindowException e) {
			System.out.println("The window is not found");
		} catch(WebDriverException e) {
			System.out.println("Webdriver exception occurred");
		}
		
	}

	@Override
	public void switchToFrame(int index) {
		try {
			driver.switchTo().frame(index);
			System.out.println("Switched to the frame "+index+" successfully");
		} catch (NoSuchFrameException e) {
			System.out.println("The frame is not found");
		} catch(WebDriverException e) {
			System.out.println("Webdriver exception occurred");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			System.out.println("Switched to the frame "+ele+" successfully");
		} catch (NoSuchFrameException e) {
			System.out.println("The frame is not found");
		}catch(WebDriverException e) {
			System.out.println("Webdriver exception occurred");
		} finally {
			takeSnap();
		}

	}

	@Override
	public void switchToFrame(String idOrName) {
		try {
			driver.switchTo().frame(idOrName);
			System.out.println("Swicthed to the frame "+idOrName+" successfully");
		} catch (NoSuchFrameException e) {
			System.out.println("The frame is not found");
		}catch(WebDriverException e) {
			System.out.println("Webdriver exception occurred");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void defaultContent() {
		try {
			driver.switchTo().defaultContent();
			System.out.println("Switched back to the base frame");
		} catch (NoSuchFrameException e) {
			System.out.println("The frame is not found");
		}catch(WebDriverException e) {
			System.out.println("Webdriver exception occurred");
		}finally {
			takeSnap();
		}
	}

	@Override
	public boolean verifyUrl(String url) {
		try {
			if(driver.getCurrentUrl().equals(url)){
					System.out.println("The given url "+url+" is correct");
			return true;
			}else {
					System.out.println("The given url "+url+" is not correct");
			}
		} catch (WebDriverException e) {
			System.out.println("Unknown error occurred");
		}
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		try {
			if(driver.getTitle().equals(title)) {
				System.out.println("The Title "+title+" is correct");
			return true;
			}else {
				System.out.println("The Title "+title+" is not correct");
			}
		} catch (WebDriverException e) {
			System.out.println("Unknown error occurred");
		}
		return false;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
		driver.close();
	}

	@Override
	public void quit() {
		driver.quit();
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The element "+ele+" clicked successfully");
		} catch (NoSuchElementException e) {
			System.out.println("The element "+ele+" is not available");
		}finally {
		takeSnap();
		}
	}

	@Override
	public void append(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			System.out.println("The data "+data+" entered successfully");
		} catch (NoSuchElementException e) {
			System.out.println("The element "+ele+" is not available");
		}finally {
		takeSnap();
		}
	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
			System.out.println("The data is cleared");
		} catch (NoSuchElementException e) {
			System.out.println("The element "+ele+" is not available");
		}finally {
		takeSnap();
		}
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data); 
			System.out.println("The data "+data+" entered successfully");
		} catch (NoSuchElementException e) {
			System.out.println("The element "+ele+" is not available");
		}finally {
		takeSnap();
		}
	}

	@Override
	public String getElementText(WebElement ele) {
		if(ele.getText()!=null)
		return ele.getText();
		else
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		String cssValue = ele.getCssValue("background-color");
		return cssValue;
	}

	@Override
	public String getTypedText(WebElement ele) {
		String attribute = ele.getAttribute("value");
		return attribute;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select sel = new Select(ele);
			sel.selectByVisibleText(value);
			System.out.println("The element "+ele+" is selected");
		} catch (NoSuchElementException e) {
			System.out.println("The element "+ele+" is not available");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select sel = new Select(ele);
			sel.selectByIndex(index);
			System.out.println("The element "+ele+" is selected");
		} catch (NoSuchElementException e) {
			System.out.println("The element "+ele+" is not available");
		}finally {
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			Select sel = new Select(ele);
			sel.selectByValue(value);
			System.out.println("The element "+ele+" is selected");
		} catch (NoSuchElementException e) {
			System.out.println("The element "+ele+" is not available");
		}finally {
			takeSnap();
		}
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		try {
			if(expectedText.equals(ele.getText())) {
				System.out.println("The expected text "+expectedText+" is correct");
				return true;
			}else {
				System.out.println("The expected text "+expectedText+" is not correct");
			}
		} catch (WebDriverException e) {
			System.out.println("Unknown error occurred");
		}finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		try {
			if(ele.getText().contains(expectedText)) {
				System.out.println("The expected text contains the actual "+expectedText);
				return true;
			}else {
				System.out.println("The expected text does not contains the actual "+expectedText);
			}
		} catch (WebDriverException e) {
			System.out.println("Unknown error occurred");
		}finally {
			takeSnap();
		}
			return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
			if(value.equals(ele.getAttribute(attribute))) {
				System.out.println("The retrieved attribute's value "+attribute+" is matching with actual value "+value);
					return true;
			}else {
				System.out.println("The retrieved attribute's value "+attribute+" is not matching with actual value "+value);
			}
		} catch (WebDriverException e) {
			System.out.println("Unknown error occurred");
		}finally {
			takeSnap();
		}
			return false;
	}

	@Override
	public boolean verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try{
			if(value.contains(ele.getAttribute(attribute))) {
				System.out.println("The retrieved attribute's value "+attribute+" is matching with actual value "+value);
		return true;
			}else {
				System.out.println("The retrieved attribute's value "+attribute+" is not matching with actual value "+value);
			}
		} catch (WebDriverException e) {
			System.out.println("Unknown error occurred");
		}finally {
			takeSnap();
		}
		return false;
		
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		try {
			if(ele.isDisplayed()) {
				System.out.println("The element "+ele+" is displayed");
				return true;
			}else {
				System.out.println("The element "+ele+" is not displayed");
			}
		} catch (WebDriverException e) {
			System.out.println("Unknown error occurred");
		}finally {
			takeSnap();
		}
			return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		if(!ele.isDisplayed())
			return true;
		else
			return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		try {
			if(ele.isEnabled()) {
				System.out.println("The element "+ele+" is enabled");
				return true;
			}else {
				System.out.println("The element "+ele+" is not enabled");
			}
		} catch (WebDriverException e) {
			System.out.println("Unknown error occurred");
		}finally {
			takeSnap();
		}
			return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		try {
			if(ele.isSelected()) {
				System.out.println("The element "+ele+" is selected");
				return true;
			}else {
				System.out.println("The elemnt "+ele+" is not selecetd");
			}
		} catch (WebDriverException e) {
			System.out.println("Unknown error occurred");
		}finally {
			takeSnap();
		}
			return false;
	}

}
