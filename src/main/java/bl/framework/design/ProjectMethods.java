package bl.framework.design;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import bl.framework.api.SeleniumBase;
import utils.ReadExcel;

public class ProjectMethods extends SeleniumBase{

public String testcaseName, testDec, author, category, dataSheetName;

@BeforeSuite
	public void beforeSuite() {
		startReport();
}

@AfterSuite
	public void afterSuite() {
		endReport();
}

@BeforeClass
	public void beforeClass() {
		assignTest(testcaseName, testDec, author, category);
}

@Parameters({"url", "username", "password"})
@BeforeMethod
	public void login(String url, String user, String pass) {
		startApp("chrome", url);
		//Login
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, user);
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, pass);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		//CRMSFA
		WebElement eleCrmSfa = locateElement("linktext", "CRM/SFA");
		click(eleCrmSfa);
	}

@AfterMethod
	public void afterMethod() {
		close();
}

@DataProvider(name = "fetchData")
public Object[][] getData1() throws IOException{
	return ReadExcel.readData(dataSheetName);
}
}
